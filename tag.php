<?php

/**
 * The template for tag page
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
get_header(); ?>
<main class="main home-page bg-light pb-4">
	<div class="container-fluid">
		<div class="row">
			<div class="banner-panel text-center" style="background-image: url(<?php echo grace_option('image_default_tag'); ?>);">
				<h1 class="wow bounceInLeft"><?php echo single_cat_title('', false); ?></h1>
				<p class="wow bounceInRight pt-2 px-3"><?php echo strip_tags(category_description()); ?></p>
			</div>
		</div>
	</div>
	<div class="container pt-4 sort bg-light">
		<div class="category-list row">
			<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; $query = new WP_Query( array('paged'=> $paged ,'showposts'=>15));
				if(have_posts()){ 
					while (have_posts()) : 
						the_post();get_template_part('/inc/content/content-tags', get_post_format());
					endwhile;wp_reset_query();
				}else{ ?>
					<div class="jumbotron container">
						<h2 class="display-3">真的很抱歉！</h2>
						<p class="lead">这里一片空白，没有任何内容，你愿意来填满它吗？</p>
							<hr class="m-y-md">
						<p>东风夜放花千树。更吹落、星如雨。宝马雕车香满路。凤箫声动，玉壶光转，一夜鱼龙舞。<br />
							蛾儿雪柳黄金缕。笑语盈盈暗香去。众里寻他千百度。蓦然回首，那人却在，灯火阑珊处。</p>
						<p class="lead">
							<a class="btn btn-primary btn-lg" href="<?php echo wp_login_url( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ); ?>" role="button">我来填满</a>
						</p>
					</div>
				<?php }?>
		</div>
		<div class="more text-center mt-4" id="categorypage">
			<?php next_posts_link( __('Load More' , 'grace') ) ; ?>
		</div>
	</div>
</main>
<?php get_footer(); ?>