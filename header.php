<?php
/**
 * 主题页眉
 * @author Seaton Jiang <seaton@vtrois.com>
 * @license MIT License
 * @version 2020.02.15
 */
?>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php wp_title( '-', true, 'right' ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="format-detection" content="telphone=no, email=no">
    <meta name="keywords" content="<?php keywords(); ?>">
    <meta name="description" itemprop="description" content="<?php description(); ?>">
    <meta name="theme-color" content="<?php echo kratos_option('g_chrome', '#282a2c'); ?>">
    <meta itemprop="image" content="<?php echo share_thumbnail_url(); ?>"/>
    <link rel="shortcut icon" href="<?php echo kratos_option('g_icon'); ?>">
    <?php wp_head(); mourning(); ?>
</head>
<?php flush(); ?>
<body>
<div class="k-header">
    <nav class="k-nav navbar navbar-expand-lg navbar-light fixed-top">
        <div class="container">
            <a class="navbar-brand" href="<?php echo get_option('home'); ?>">
                <?php
                    if (kratos_option('g_logo')){
                        echo '<img src="' . kratos_option('g_logo') . '">';
                    }else{
                        bloginfo('name');
                    }
                ?>
            </a>
            <?php if ( has_nav_menu('header_menu') ) { ?>
            <button class="navbar-toggler navbar-toggler-right" id="navbutton" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="line first-line"></span>
                <span class="line second-line"></span>
                <span class="line third-line"></span>
            </button>
            <?php }
                wp_nav_menu( array(
                    'theme_location'  => 'header_menu',
                    'depth'           => 1,
                    'container'       => 'div',
                    'container_class' => 'collapse navbar-collapse',
                    'container_id'    => 'navbarResponsive',
                    'menu_class'      => 'navbar-nav ml-auto',
                    'walker'          => new WP_Bootstrap_Navwalker(),
                ) );
            ?>
        </div>
    </nav><!-- .k-nav -->
    <div class="banner">
        <div class="overlay"></div>
        <div class="content text-center" style="background-image: url(<?php 
            if(!kratos_option('top_img')){
                $img = get_template_directory_uri() . '/assets/img/background.png';
            } else {
                $img = kratos_option('top_img', get_template_directory_uri() . '/assets/img/background.png');
            }
            echo $img; ?>);">
            <div class="introduce">
                <?php
                    if (is_category() || is_tag()) {
                        echo '<div class="title">' . single_cat_title('', false) . '</div>';
                        echo '<div class="mate">' . strip_tags(category_description()) . '</div>';
                    } else {
                        echo '<div class="title">' . kratos_option('top_title', 'Kratos') . '</div>';
                        echo '<div class="mate">' . kratos_option('top_describe', '一款专注于用户阅读体验的响应式博客主题') . '</div>';
                    }
                ?>
            </div>
        </div>
    </div><!-- .banner -->
</div><!-- .k-header -->
<?php

/**
 * The template for header
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
?>
<!DOCTYPE html>
<html lang="zh-hans">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="robots" content="index,follow">
		<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="format-detection" content="telphone=no, email=no">
		<title><?php wp_title( '-', true, 'right' ); ?></title>
		<meta itemprop="title" property="og:title" content="<?php wp_title( '-', true, 'right' ); ?>">
		<meta property="og:type" content="<?php grace_keywords(); ?>">
		<meta property="og:description" content="<?php grace_description(); ?>">
		<meta property="og:url" content="<?php echo grace_current_url(); ?>">
		<meta property="og:site_name" content="<?php echo get_bloginfo('name');?>">
		<meta property="og:image" content="<?php if (is_home()) { echo grace_option('image_default_share'); }else{ echo grace_thumbnail_url(); } ?>">
		<meta name="description" content="<?php grace_description(); ?>">
		<meta name="keywords" content="<?php grace_keywords(); ?>">
		<meta itemprop="image" content="<?php if (is_home()) { echo grace_option('image_default_share'); }else{ echo grace_thumbnail_url(); } ?>"/>
		<meta name="description" itemprop="description" content="<?php grace_description(); ?>" />
		<link rel="icon" type="image/x-icon" href="<?php echo grace_option('global_ico'); ?>">
		<link rel="canonical" href="<?php echo grace_current_url(); ?>"/>
		<?php wp_head(); ?>
		<?php wp_print_scripts('jquery'); ?>
	</head>
	<body>
		<header>			
		    <nav class="navbar navbar-expand-md fixed-top navbar-dark site-header">
		    	<?php if ( has_nav_menu('header_menu') ) {?>
		        <button class="navbar-toggler p-0 border-0 nav-list" type="button" data-toggle="menu">
		            <span class="line first-line"></span>
		            <span class="line second-line"></span>
		            <span class="line third-line"></span>
		        </button> 
		        <?php } ?>
	        	<?php $site_logo = grace_option('site_logo');?>
				<?php if ( !empty( $site_logo ) ) {?>
		        	<a class="navbar-brand navbar-full" href="<?php echo home_url(); ?>"><img class="site-logo" src="<?php echo $site_logo; ?>"/></a>
		        <?php }else{?>
		        	<a class="navbar-brand navbar-full" style="font-size: 1.6rem;" href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>
		        <?php } ?>
		        <?php if ( is_user_logged_in() ) { ?> 
					<a class="navbar-toggler avatars p-0 border-0" href="<?php echo admin_url(); ?>">
						<?php global $current_user; wp_get_current_user(); echo get_avatar( $current_user->user_email ); ?>
			        </a>
				<?php } else { ?> 
					<a class="navbar-toggler p-0 border-0" href="<?php if(grace_option('global_login')){ echo grace_option('global_login');} else {echo wp_login_url( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );}?>">
			            <span class="grace v3-users"></span>
			        </a>
				<?php } ?>
		        <div class="navbar-collapse full-collapse collapse ">
        		<?php wp_nav_menu( array('theme_location' => 'header_menu', 'depth' => 2, 'container' => null, 'menu_class' => 'navbar-nav mr-auto', 'walker' => new WP_Bootstrap_Navwalker())); ?>
		            <form class="form-inline d-none d-lg-block" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
		              <input class="form-control mr-sm-2" type="search" id="search" name="s" placeholder="Search" aria-label="Search">
		              <button class="btn-search" type="submit">Search</button>
		            </form>
					<?php if ( is_user_logged_in() ) { ?> 
			            <div class="nav-brush ml-3 mr-2 d-none d-md-block">
							<?php if ( is_single() || is_page() ) { ?>
								<?php echo edit_post_link('<span class="grace v3-brush"></span>'); ?>
							<?php } else { ?>
			                <a href="<?php echo admin_url(); ?>post-new.php"><span class="grace v3-editor"></span></a>
			                <?php } ?>
			            </div>
					<?php } ?>
		            <div class="nav-users ml-1 mr-0 d-none d-md-block ">
			        <?php if ( is_user_logged_in() ) { ?>
			        	<ul class="navbar-nav">
				        	<li class="nav-item dropdown">
								<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="avatars dropdown-toggle nav-link" href="#">
									<?php global $current_user; wp_get_current_user(); echo get_avatar($current_user->user_email); ?>
						        </a>
						        <ul class="dropdown-menu user-login-menu"> 
						        	<li class="nav-item"><a title="个人中心" href="<?php echo admin_url(); ?>" class="dropdown-item"><i class="fa fa-user-o mr-1"></i>个人中心</a></li>
						        	<li class="nav-item"><a title="退出" href="<?php echo wp_logout_url( home_url() ); ?>" class="dropdown-item"><i class="fa fa-sign-out mr-1"></i>退出</a></li>
						        </ul>
					        </li>
				        </ul>
					<?php } else { ?> 
						<a href="<?php if(grace_option('global_login')){ echo grace_option('global_login');} else {echo wp_login_url( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );}?>">
				            <span class="text-gray">登录</span>
				        </a>
				        <a class="ml-2" href="<?php if(grace_option('global_login')){ echo grace_option('global_login');} else {echo wp_registration_url();}?>">
				            <span class="text-gray">注册</span>
				        </a>
					<?php } ?>
		            </div>
		        </div>
		    </nav>
		</header>
		