<?php 
/**
 * 404 模板
 * @author Seaton Jiang <seaton@vtrois.com>
 * @license MIT License
 * @version 2020.02.15
 */

get_header(); ?>
<div class="k-main" style="background:#ffffff">
    <div class="container">
        <div class="row">
            <div class="col-12 page404">
                <div class="thumbnail" style="background-image: url(<?php
                if(!kratos_option('g_404')){
                    $img = get_template_directory_uri() . '/assets/img/404.jpg';
                } else {
                    $img = kratos_option('g_404', get_template_directory_uri() . '/assets/img/404.jpg');
                }
                echo $img; ?>">
					<div class="overlay"></div>
				</div>
                <div class="content text-center">
                    <div class="title pt-4"><?php _e('很抱歉，你访问的页面不存在', 'kratos'); ?></div>
                    <div class="subtitle pt-4"><?php _e('可能是输入地址有误或该地址已被删除', 'kratos'); ?></div>
                    <div class="action pt-4">
                        <a href="javascript:history.go(-1)" class="btn btn-outline-primary back-prevpage"><?php _e('返回上页', 'kratos'); ?></a>
                        <a href="<?php echo get_option('home'); ?>" class="btn btn-outline-primary ml-3 back-index"><?php _e('返回主页', 'kratos'); ?></a>
                    </div>
                </div>
            </div><!-- .page404 -->
        </div>
    </div>
</div><!-- .k-main -->
<?php get_footer(); ?><?php

/**
 * The template for 404 page
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
get_header(); ?>
<main class="main page404 bg-white py-4">
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="thumbnail" style="background-image: url(<?php echo grace_option('image_default_404'); ?>">
					<div class="overlay"></div>
				</div>
			</div>
			<div class="col-md-12 text-center meta py-3">
				<h2><?php _e( 'Sorry, that page you visited does not exist' , 'grace' ); ?></h2>
            	<h3 class="pt-3"><?php _e( 'It may be the incorrect input address or the address has been deleted' , 'grace' ); ?></h3>
            	<p class="pt-3">
            		<a href="javascript:history.go(-1)" class="btn btn-outline-primary back-prevpage"><?php _e( 'Return' , 'grace' ); ?></a>
            		<a href="<?php echo get_option('home'); ?>" class="btn btn-outline-primary ml-3 back-index"><?php _e( 'Homepage' , 'grace' ); ?></a>
            	</p>
			</div>
		</div>
	</div>
</main>
<?php get_footer(); ?>