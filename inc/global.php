<?php

/**
 * Add the default avatar image
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 * @global grace_option('image_default_gravatar')
 */
add_filter( 'avatar_defaults', 'grace_gravatar' );

function grace_gravatar ($avatar_defaults) {
    $azavatar = grace_option('image_default_gravatar');
    $myavatar = ($azavatar) ? $azavatar : get_template_directory_uri() . '/images/avatar.png' ;  
    $avatar_defaults[$myavatar] = "Grace Gravatar";  
    return $avatar_defaults;  
}

/**
 * Nofollow the comments and images link
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
add_filter( 'comments_popup_link_attributes', 'grace_nofollow_comments_link' );

function grace_nofollow_comments_link() {
	return' rel="nofollow"';
}

add_filter( 'the_content', 'grace_nofollow_images_link' );

function grace_nofollow_images_link( $content ) {
    $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>";
    if(preg_match_all("/$regexp/siU", $content, $matches, PREG_SET_ORDER)) {
        if( !empty($matches) ) {
            $srcUrl = get_option('siteurl');
            for ($i=0; $i < count($matches); $i++)
            {
                $tag = $matches[$i][0];
                $tag2 = $matches[$i][0];
                $url = $matches[$i][0];
                $noFollow = '';
                $pattern = '/target\s*=\s*"\s*_blank\s*"/';
                preg_match($pattern, $tag2, $match, PREG_OFFSET_CAPTURE);
                if( count($match) < 1 )
                    $noFollow .= ' target="_blank" ';
                $pattern = '/rel\s*=\s*"\s*[n|d]ofollow\s*"/';
                preg_match($pattern, $tag2, $match, PREG_OFFSET_CAPTURE);
                if( count($match) < 1 )
                    $noFollow .= ' rel="nofollow" ';
                $pos = strpos($url,$srcUrl);
                if ($pos === false) {
                    $tag = rtrim ($tag,'>');
                    $tag .= $noFollow.'>';
                    $content = str_replace($tag2,$tag,$content);
                }
            }
        }
    }
    $content = str_replace(']]>', ']]>', $content);
    return $content;
}

/**
 * Title Settings
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 * @return string title
 */
add_filter( 'wp_title', 'grace_wp_title', 10, 2 );

function grace_wp_title( $title, $sep ) {
  global $paged, $page;
  if ( is_feed() )
    return $title;
  $title .= get_bloginfo( 'name' );
  $site_description = get_bloginfo( 'description', 'display' );
  if ( $site_description && ( is_home() || is_front_page() ) )
    $title = "$title $sep $site_description";
  if ( $paged >= 2 || $page >= 2 )
    $title = "$title $sep " . sprintf( __( 'Page %s', 'grace' ), max( $paged, $page ) );
  return $title;
}

/**
 * Keyword Settings
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 * @global grace_option('global_keywords')
 */
function grace_keywords(){
  if( is_home() || is_front_page() ){ echo grace_option('global_keywords'); }
  elseif( is_category() ){ single_cat_title(); }
  elseif( is_single() ){
    if ( has_tag() ) {
    	foreach((get_the_tags()) as $tag ) {
    		echo $tag->name.','; 
    	} 
    }
    foreach((get_the_category()) as $category) { 
    	echo $category->cat_name; 
    } 
  }
  elseif( is_search() ){ the_search_query(); }
  else{ echo trim(wp_title('',FALSE)); }
}

/**
 * Description Settings
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 * @global grace_option('global_description')
 */
function grace_description(){
  if( is_home() || is_front_page() ){ echo trim(grace_option('global_description')); }
  elseif( is_category() ){ $description = strip_tags(category_description());echo trim($description);}
    elseif( is_single() ){ 
    if(get_the_excerpt()){
      echo get_the_excerpt();
    }else{
      global $post;
      $description = trim( str_replace( array( "\r\n", "\r", "\n", "　", " "), " ", str_replace( "\"", "'", strip_tags( $post->post_content ) ) ) );
      echo mb_substr( $description, 0, 220, 'utf-8' );
    }
  }
  elseif( is_tag() ){  $description = strip_tags(tag_description());echo trim($description); }
  else{ $description = strip_tags(term_description());echo trim($description); }
}

/**
 * Current the page url
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 * @return string url
 */
function grace_current_url(){
    global $wp;
    return get_option( 'permalink_structure' ) == '' ? add_query_arg( $wp->query_string, '', home_url( $wp->request ) ) : home_url( add_query_arg( array(), $wp->request ) );
}

/**
 * Html Compress
 *
 * Sed to compress the page size and improve loading speed.
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
if (grace_option('global_html')) {
  add_action('get_header', 'grace_compress_html');
  add_filter( "the_content", "grace_unCompress");
}

function grace_compress_html(){
  function grace_compress_html_main ($buffer){
    $initial=strlen($buffer);
    $buffer=explode("<!--Grace-Compress-html-->", $buffer);
    $count=count ($buffer);
    for ($i = 0; $i <= $count; $i++){
        if (stristr($buffer[$i], '<!--Grace-Compress-html-no-compression-->')) {
            $buffer[$i]=(str_replace("<!--Grace-Compress-html-no-compression-->", " ", $buffer[$i]));
        } else {
            $buffer[$i]=(str_replace("\t", " ", $buffer[$i]));
            $buffer[$i]=(str_replace("\n\n", "\n", $buffer[$i]));
            $buffer[$i]=(str_replace("\n", "", $buffer[$i]));
            $buffer[$i]=(str_replace("\r", "", $buffer[$i]));
            $buffer[$i]=(str_replace("\v", "", $buffer[$i]));
            $buffer[$i]=(str_replace("\f", "", $buffer[$i]));
            while (stristr($buffer[$i], '  ')) {
                $buffer[$i]=(str_replace("  ", " ", $buffer[$i]));
            }
        }
        $buffer_out.=$buffer[$i];
    }
    $final=strlen($buffer_out);   
    $savings=($initial-$final)/$initial*100;   
    $savings=round($savings, 2);   
    $buffer_out.="\n<!-- Initial: $initial bytes; Final: $final bytes; Reduce：$savings% :D -->";   
    return $buffer_out;
  }
  ob_start("grace_compress_html_main");
}

function grace_unCompress($content) {
    if(preg_match_all('/(crayon-|<\/pre>)/i', $content, $matches)) {
        $content = '<!--Grace-Compress-html--><!--Grace-Compress-html-no-compression-->'.$content;
        $content.= '<!--Grace-Compress-html-no-compression--><!--Grace-Compress-html-->';
    }
    return $content;
}

/**
 * Page Permalink
 *
 * Open the pseudo-static rule of the page.
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
add_action('init', 'grace_page_permalink', -1);
function grace_page_permalink() {
    if (grace_option('page_html')){
        global $wp_rewrite;
        if ( !strpos($wp_rewrite->get_page_permastruct(), '.html')){
            $wp_rewrite->page_structure = $wp_rewrite->page_structure . '.html';
        }
    }
}
/**
 * Carousel
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
function grace_carousel(){
  $output = '';
  $carousel = grace_option("carousel_status") ? grace_option("carousel_status") : 0;
  if($carousel){
    for($i=1; $i<6; $i++){
        $grace_carousel_img{$i} = grace_option("carousel_img_{$i}") ? grace_option("carousel_img_{$i}") : "";
        $grace_carousel_url{$i} = grace_option("carousel_url_{$i}") ? grace_option("carousel_url_{$i}") : "";
        $grace_carousel_title{$i} = grace_option("carousel_title_{$i}") ? grace_option("carousel_title_{$i}") : "";
        $grace_carousel_meta{$i} = grace_option("carousel_meta_{$i}") ? grace_option("carousel_meta_{$i}") : "";
        if($grace_carousel_img{$i} ){
            $carousel_img[] = $grace_carousel_img{$i};
            $carousel_url[] = $grace_carousel_url{$i};
            $carousel_title[] = $grace_carousel_title{$i};
            $carousel_meta[] = $grace_carousel_meta{$i};
        }
    }
    $count = count($carousel_img);
    for($i=0; $i<$count; $i++){
        $output .= '<li style="background-image: url('.$carousel_img[$i].');"><div class="overlay-gradient"></div><div class="container"><div class="row"><div class="col-lg-6 slider-text"><div class="slider-text-inner"><div class="desc"><h2>'.$carousel_title[$i].'</h2><p class="meta">'.$carousel_meta[$i].'</p><p class="more"><a href="'.$carousel_url[$i].'" class="btn btn-outline-primary">'.__('Read More','grace').'</a></p></div></div></div></div></div></li>';
    };
  };
  echo $output;
}

/**
 * Timeago
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
function timeago($ptime){
  $ptime = strtotime($ptime);
  $etime = time() - $ptime;
  if($etime < 1)
    return'刚刚';
  $interval = array(
    12*30*24*60*60 => ' 年前（'.date('m月d日',$ptime).'）',
    30*24*60*60 => ' 个月前（'.date('m月d日',$ptime).'）',
    7*24*60*60 => ' 周前（'.date('m月d日',$ptime).'）',
    24*60*60 => ' 天前（'.date('m月d日',$ptime).'）',
    60*60 => ' 小时前（'.date('m月d日',$ptime).'）',
    60 => ' 分钟前（'.date('m月d日',$ptime).'）',
    1 => ' 秒前（'.date('m月d日',$ptime).'）',
  );
  foreach($interval as$secs=>$str){
    $d=$etime/$secs;
    if($d>=1){
    $r=round($d);
    return$r.$str;
    }
  };
}

/**
 * String cut
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
function grace_string_cut($string, $sublen, $start = 0, $code = 'UTF-8') {
   if($code == 'UTF-8') {
      $pa = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/";
      preg_match_all($pa, $string, $t_string);
      if(count($t_string[0]) - $start > $sublen) return join('', array_slice($t_string[0], $start, $sublen)) . "...";
      return join('', array_slice($t_string[0], $start, $sublen));
  } else {
    $start = $start * 2;
    $sublen = $sublen * 2;
    $strlen = strlen($string);
    $tmpstr = '';
    for($i = 0; $i < $strlen; $i++) {
    if($i >= $start && $i < ($start + $sublen)) {
        if(ord(substr($string, $i, 1)) > 129) $tmpstr .= substr($string, $i, 2);
        else $tmpstr .= substr($string, $i, 1);
   }
      if(ord(substr($string, $i, 1)) > 129) $i++;
    }
      return $tmpstr;
  }
}

/**
 * Latest comments
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
function grace_latest_comments($list_number=5, $cut_length=50) {
  global $wpdb,$output;
  $comments = $wpdb->get_results("SELECT DISTINCT ID, post_title, post_password, comment_ID, comment_post_ID, comment_author, comment_date_gmt, comment_approved, comment_type,comment_author_url,comment_author_email, comment_content AS com_excerpt FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID = $wpdb->posts.ID) WHERE comment_approved = '1' AND comment_type = '' AND user_id != '1' AND post_password = '' ORDER BY comment_date_gmt DESC LIMIT $list_number");
  foreach ($comments as $comment) {
    $output .= '<a href="'. esc_url( get_comment_link( $comment ) ) .'" title="'. __( 'Publish in:' , 'grace').' '. $comment->post_title .'"><div class="meta clearfix"><div class="avatar float-left">'.get_avatar( $comment, 60 ).'</div><div class="profile d-block"><span class="date">发布于 '.timeago($comment->comment_date_gmt).'</span><span class="message d-block">'.convert_smilies(grace_string_cut(strip_tags($comment->com_excerpt), $cut_length)).'</span></div></div></a>';
  }
  return $output;
}

/**
 * Add the smilies button for admin
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
add_action('media_buttons_context', 'grace_smilies_custom_button');
function grace_smilies_custom_button($context) {
    $context .= '<style>.smilies-wrap{background:#fff;border: 1px solid #ccc;box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.24);padding: 10px;position: absolute;top: 60px;width: 400px;display:none}.smilies-wrap img{height:24px;width:24px;cursor:pointer;margin-bottom:5px} .is-active.smilies-wrap{display:block}</style><a id="insert-media-button" style="position:relative" class="button insert-smilies add_smilies" data-editor="content" href="javascript:;"><span class="dashicons dashicons-smiley" style="line-height: 26px;"></span>'.__( 'Add the expression', 'grace' ).'</a><div class="smilies-wrap">'. grace_get_wpsmiliestrans() .'</div><script>jQuery(document).ready(function(){jQuery(document).on("click", ".insert-smilies",function() { if(jQuery(".smilies-wrap").hasClass("is-active")){jQuery(".smilies-wrap").removeClass("is-active");}else{jQuery(".smilies-wrap").addClass("is-active");}});jQuery(document).on("click", ".add-smily",function() { send_to_editor(" " + jQuery(this).data("smilies") + " ");jQuery(".smilies-wrap").removeClass("is-active");return false;});});</script>';
    return $context;
}

function grace_get_wpsmiliestrans(){
    global $wpsmiliestrans;
    global $output;
    $wpsmilies = array_unique($wpsmiliestrans);
    foreach($wpsmilies as $alt => $src_path){
        $output .= '<a class="add-smily" data-smilies="'.$alt.'"><img class="wp-smiley" src="'.get_bloginfo('template_directory').'/static/images/smilies/'.rtrim($src_path, "png").'png" /></a>';
    }
    return $output;
}

/**
 * The article heat
 * 
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 */
function most_comm_posts($days=30, $nums=5, $orderby) {
    global $wpdb;
    date_default_timezone_set("PRC");
    $today = date("Y-m-d H:i:s");
    $daysago = date( "Y-m-d H:i:s", strtotime($today) - ($days * 24 * 60 * 60) );
    $result = $wpdb->get_results("SELECT post_content,post_excerpt, comment_count, ID, post_title, post_date FROM $wpdb->posts WHERE post_date BETWEEN '$daysago' AND '$today' and post_type='post' and post_status='publish' ORDER BY comment_count DESC LIMIT 0 , $nums");
    $output = '';
    if(empty($result)) {
        $output = '<li>暂时没有数据</li>';
    } else {
    	if($orderby == "thumbnail"){
    		foreach ($result as $topten) {
	            $postid = $topten->ID;
	            $content = $topten->post_content;
	            $title = $topten->post_title;
	            $commentcount = $topten->comment_count;
	            
	            $excerpt = $topten->post_excerpt;
	            if(empty($excerpt)){
	            	$text = strip_shortcodes( $content );
					$text = str_replace(']]>', ']]&gt;', $text);
					$excerpt = wp_trim_words($text,50);
	            }
				
			    $img_preg = "/<img (.*?) src=\"(.+?)\".*?>/";
			    preg_match($img_preg,$content,$img_src);
			    $img_count=count($img_src) - 1;
			    $img_val = "";
			    if (isset($img_src[$img_count]))
			      $img_val = $img_src[$img_count];
			    $thumbnail_img_src = "";
			    if(!empty($img_val)){
			    	$thumbnail_img_src = $img_val;
			    } else {
			    	$thumbnail_img_src = grace_option('image_thumbnail_index');
			    }
	            if ($commentcount >= 0) {
	                $output .= '<a class="list-group-item" title="'. $title .'" href="'.get_permalink($postid).'" rel="bookmark"><div class="thumbnail-list">';
	                $output .= '<div class="thumbnail_img"><img width="480" height="300" src="'. $thumbnail_img_src .'"/></div>';
	                $output .= '<div class="item-content"><p class="item-title">'. strip_tags($title) .'</p>';
	                $output .= '<p class="item-excerpt">'. $excerpt .'</p>';
	                $output .= '</div></div></a>';
	            }
	        }
    	}else{
	        foreach ($result as $topten) {
	            $postid = $topten->ID;
	            $title = $topten->post_title;
	            $commentcount = $topten->comment_count;
	            if ($commentcount >= 0) {
	                $output .= '<a class="list-group-item" title="'. $title .'" href="'.get_permalink($postid).'" rel="bookmark"><div class="wrap-content"><i class="fa  fa-book"></i> ';
	                $output .= strip_tags($title);
	                $output .= '</div></a>';
	            }
	        }
        }
    }
    echo $output;
}

/**
 * 按文章浏览量进行排序 
 */
function most_comm_views($days=30, $nums=5, $orderby) {
    global $wpdb;
    date_default_timezone_set("PRC");
    $today = date("Y-m-d H:i:s");
    $daysago = date( "Y-m-d H:i:s", strtotime($today) - ($days * 24 * 60 * 60) );
    $result = $wpdb->get_results("SELECT post_content,post_excerpt, comment_count, ID, post_title, post_date FROM $wpdb->posts WHERE post_date BETWEEN '$daysago' AND '$today' and post_type='post' and post_status='publish' ORDER BY comment_count DESC LIMIT 0 , $nums");
    $result = $wpdb->get_results("SELECT post_content, post_excerpt, ID, post_title FROM $wpdb->posts , $wpdb->postmeta WHERE  post_date BETWEEN '$daysago' AND '$today' and post_type='post' and post_status='publish' AND ID = $wpdb->postmeta.post_id AND $wpdb->postmeta.meta_key = 'views' ORDER BY $wpdb->postmeta.meta_value + 0 DESC LIMIT 0,$nums");
    $output = '';
    if(empty($result)) {
        $output = '<li>暂时没有数据</li>';
    } else {
    	if($orderby == "thumbnail"){
    		foreach ($result as $topten) {
	            $postid = $topten->ID;
	            $content = $topten->post_content;
	            $title = $topten->post_title;
	            
	            $excerpt = $topten->post_excerpt;
	            if(empty($excerpt)){
	            	$text = strip_shortcodes( $content );
					$text = str_replace(']]>', ']]&gt;', $text);
					$excerpt = wp_trim_words($text,50);
	            }
				
			    $img_preg = "/<img (.*?) src=\"(.+?)\".*?>/";
			    preg_match($img_preg,$content,$img_src);
			    $img_count=count($img_src) - 1;
			    $img_val = "";
			    if (isset($img_src[$img_count]))
			      $img_val = $img_src[$img_count];
			    $thumbnail_img_src = "";
			    if(!empty($img_val)){
			    	$thumbnail_img_src = $img_val;
			    } else {
			    	$thumbnail_img_src = grace_option('image_thumbnail_index');
			    }
			    
                $output .= '<a class="list-group-item" title="'. $title .'" href="'.get_permalink($postid).'" rel="bookmark"><div class="thumbnail-list">';
                $output .= '<div class="thumbnail_img"><img width="480" height="300" src="'. $thumbnail_img_src .'"/></div>';
                $output .= '<div class="item-content"><p class="item-title">'. strip_tags($title) .'</p>';
                $output .= '<p class="item-excerpt">'. $excerpt .'</p>';
                $output .= '</div></div></a>';
	        }
    	}else{
	        foreach ($result as $topten) {
	            $postid = $topten->ID;
	            $title = $topten->post_title;
	            
                $output .= '<a class="list-group-item" title="'. $title .'" href="'.get_permalink($postid).'" rel="bookmark"><div class="wrap-content"><i class="fa  fa-book"></i> ';
                $output .= strip_tags($title);
                $output .= '</div></a>';
	        }
        }
    }
    echo $output;
}

/**
 * Paging
 * 
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 */
function grace_pages($range = 5){
    global $paged, $wp_query,$max_page;
    if ( !$max_page ) {$max_page = $wp_query->max_num_pages;}
    if($max_page > 1){if(!$paged){$paged = 1;}
    echo "<div class='text-center' id='page-footer'><ul class='pagination'>";
        if($paged != 1){
            echo "<li><a href='" . get_pagenum_link(1) . "' aria-label='Previous' title='首页'>&laquo;</a></li>";
        }
        if($paged>1) echo '<li><a href="' . get_pagenum_link($paged-1) .'" class="prev" title="上一页">&lt;</a></li>';
        if($max_page > $range){
            if($paged < $range){
                for($i = 1; $i <= ($range + 1); $i++){
                    echo "<li"; if($i==$paged)echo " class='active'";echo "><a href='" . get_pagenum_link($i) ."'>$i</a></li>";
                }
            }
            elseif($paged >= ($max_page - ceil(($range/2)))){
                for($i = $max_page - $range; $i <= $max_page; $i++){
                    echo "<li";
                    if($i==$paged)
                        echo " class='active'";echo "><a href='" . get_pagenum_link($i) ."'>$i</a></li>";
                }
            }
            elseif($paged >= $range && $paged < ($max_page - ceil(($range/2)))){
                for($i = ($paged - ceil($range/2)); $i <= ($paged + ceil(($range/2))); $i++){
                    echo "<li";
                    if($i==$paged)echo " class='active'";
                    echo "><a href='" . get_pagenum_link($i) ."'>$i</a></li>";
                }
            }
        }
        else{
            for($i = 1; $i <= $max_page; $i++){
                echo "<li";
                if($i==$paged)echo " class='active'";
                echo "><a href='" . get_pagenum_link($i) ."'>$i</a></li>";
            }
        }
        if($paged<$max_page) echo '<li><a href="' . get_pagenum_link($paged+1) .'" class="next" title="下一页">&gt;</a></li>';
        if($paged != $max_page){
            echo "<li><a href='" . get_pagenum_link($max_page) . "' aria-label='Next' title='尾页'>&raquo;</a></li>";
        }
        echo "</ul></div>";
    }
}