<?php

/**
 * The template for single alpha
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
?>
<main class="main alpha-content bg-light pb-4">
	<?php if (have_posts()) : the_post(); update_post_caches($posts);$shareData = array( 'url' => get_permalink(), 'title' => $post->post_title, 'excerpt' => get_the_excerpt(), 'img' => grace_thumbnail_url(), ); wp_localize_script( 'share', 'sr', $shareData );?>
	    <div class="container pt-4">
			<div class="row">
				<div class="container">
					<?php if(grace_option("is_shared")){
							require_once( get_template_directory() . '/inc/single/share_slider.php');
						}
					?>					
					<div class="row">
						<article class="col-lg-8">
							<div class="d-none d-sm-block article-nav">
								<nav aria-label="breadcrumb">
								  <ol class="breadcrumb">
								    <li class="breadcrumb-item"><a href="<?php echo get_option('home'); ?>"><i class="fa fa-home mr-1"></i><?php _e('Home', 'grace'); ?></a></li>
								    <li class="breadcrumb-item"><?php $cats = get_the_category(); $cat = $cats[0]; echo get_category_parents($cat->term_id,true,''); ?></li>
								    <li class="breadcrumb-item active" aria-current="page"><?php _e('Article', 'grace'); ?></li>
								  </ol>
								</nav>
							</div>
							<div class="article article-content-img">								
								<div class="meta text-center sinale-alpha">
									<h1><?php the_title(); ?></h1>
									<div class="about pt-md-2 mb-3">
										<span class="d-inline-block"><a href="<?php echo get_month_link( get_the_date('Y'), get_the_date('m') ); ?>"><i class="grace v3-activity"></i> <?php echo get_the_date(); ?></a></span>
										<span class="d-none d-md-inline-block"><i class="grace v3-interactive"></i> <?php comments_number('0', '1', '%'); ?>&nbsp;<?php _e('Comments' , 'grace'); ?></span>
										<span class="d-inline-block"><i class="grace v3-browse"></i> <?php echo grace_get_post_views(); ?>&nbsp;<?php _e('Views' , 'grace'); ?></span>
										<span class="d-inline-block"><i class="grace v3-praise"></i> <?php if( get_post_meta($post->ID,'love',true) ){ echo num2tring(get_post_meta($post->ID,'love',true)); } else { echo '0'; }?>&nbsp;<?php _e('Thumb' , 'grace'); ?></span>
									</div>
								</div>
								<div class="content"><?php the_content(); ?></div>
								<div class="copyright mt-3 clearfix">
									<div class="tags float-left d-none d-md-block d-lg-block mt-3">
										<span class="fa fa-tags"></span>
										<?php if ( get_the_tags() ) { the_tags('', ' ', ''); } else{ echo '<a>' . __( 'None' , 'grace') . '</a>';  }?>
									</div>
									<div class="float-right mt-3">
										<span id="copyright"><?php _e('© The copyright belongs to the author','grace'); ?></span>
									</div>
								</div>
							</div>
							<?php require_once( get_template_directory() . '/inc/single/nav-next-link.php'); ?>
							<?php require_once( get_template_directory() . '/inc/author.php'); ?>
							<?php comments_template(); ?>
						</article>
						<aside class="aside-widget col-lg-4 d-none d-lg-block d-xl-block">
							<div id="stickysingle">
								<?php dynamic_sidebar('sidebar_single'); ?>
							</div>
						</aside>
					</div>
				</div>
			</div>
		</div>
	<?php endif;?>
</main>
