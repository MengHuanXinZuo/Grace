<?php
	$prev_post = get_previous_post(TRUE);
	$next_post = get_next_post(TRUE);
	if(!empty($prev_post) || !empty($next_post)){
?>
<nav class="navigation post-navigation clearfix" role="navigation">
	<div class="nav-previous clearfix">
		<a title="<?php echo $prev_post->post_title;?>" <?php if(!empty($prev_post)):?> href="<?php echo get_permalink($prev_post->ID);?>" <?php endif; ?> >								
			<?php if(!empty($prev_post)){?>
				<span class="nav-span d-inline-block">
					<span class="d-block"><i class="fa fa-hand-o-left mr-2"></i>上一篇</span>
					<span class="d-md-block wrap-content"><?php echo $prev_post->post_title;?></span>
				</span>
			<?php }else{ ?>
				<span class="nav-span d-inline-block">
					<span class="d-block"><i class="fa fa-smile-o mr-1"></i>没有了</span>
					<span class="d-md-block wrap-content">已经是最后的文章</span>
				</span>
			<?php } ?>
		</a>
	</div>	

	<div class="nav-next">
		<a title="<?php echo $next_post->post_title;?>" <?php if(!empty($next_post)):?> href="<?php echo get_permalink($next_post->ID);?>" <?php endif; ?>>
			<?php if(!empty($next_post)){?>
				<span class="nav-span d-inline-block">
					<span class="d-block">下一篇 <i class="fa fa-hand-o-right mr-2"></i></span>
					<span class="d-md-block wrap-content"><?php echo $next_post->post_title;?></span>
				</span>
			<?php }else{ ?>
				<span class="nav-span d-inline-block">
					<span class="d-block"><i class="fa fa-smile-o mr-1"></i>没有了</span>
					<span class="d-md-block wrap-content">已经是最后的文章</span>
				</span>
			<?php } ?>
		</a>
	</div>
</nav>
<?php } ?>