<?php

?>

<div class="share-group d-none d-md-block d-lg-block mb-3">
	<a href="javascript:;" class="plain twitter" onclick="share('qq');" rel="nofollow">
		<div class="wrap">
			<i class="grace v3-qq" style="color: dodgerblue;"></i>
		</div>
	</a>
	<a href="javascript:;" class="plain weibo" onclick="share('weibo');" rel="nofollow">
		<div class="wrap">
			<i class="grace v3-weibo" style="color: deeppink;"></i>
		</div>
	</a>
	<a href="javascript:;" class="plain weixin pop" rel="nofollow">
		<div class="wrap">
			<i class="grace v3-weixin" style="color: green;"></i>
		</div>
		<div class="share-int">
			<div class="qrcode" data-url="<?php the_permalink() ?>"></div>
			<p>微信“扫一扫”，点击右上角分享</p>
		</div>
	</a>
	<a href="javascript:;" class="plain qzone style-plain" onclick="share('qzone');" rel="nofollow">
		<div class="wrap">
			<i class="grace v3-qzone" style="color: orange;"></i>
		</div>
	</a>
	<a href="javascript:;" class="plain twitter style-plain" onclick="share('twitter');" rel="nofollow">
		<div class="wrap">
			<i class="grace v3-twitter" style="color: cornflowerblue;"></i>
		</div>
	</a>
</div>
