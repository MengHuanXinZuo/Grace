<?php

/**
 * The template for single gamma
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
?>
<main class="main alpha-content bg-light pb-4">
<?php if (have_posts()) : the_post(); update_post_caches($posts);$shareData = array( 'url' => get_permalink(), 'title' => $post->post_title, 'excerpt' => get_the_excerpt(), 'img' => grace_thumbnail_url(), ); wp_localize_script( 'share', 'sr', $shareData );?>
	<div class="container-fluid">
		<div class="post-bar row">
			<div class="bg-thumbnail" style="background-image:url(<?php echo grace_thumbnail_url(); ?>)">
			</div>
			<div class="meta text-center text-white">
				<h1><?php the_title(); ?></h1>
				<div class="about pt-2 pt-md-3">
					<span class="d-inline-block"><a style="color: #FFFFFF;" href="<?php echo get_month_link( get_the_date('Y'), get_the_date('m') ); ?>"><i class="grace v3-activity"></i> <?php echo get_the_date(); ?></a></span>
					<span class="d-none d-md-inline-block"><i class="grace v3-interactive"></i> <?php comments_number('0', '1', '%'); ?> <?php _e('Comments' , 'grace'); ?></span>
					<span class="d-inline-block"><i class="grace v3-browse"></i> <?php echo grace_get_post_views(); ?> <?php _e('Views' , 'grace'); ?></span>
					<span class="d-inline-block"><i class="grace v3-praise"></i> <?php if( get_post_meta($post->ID,'love',true) ){ echo num2tring(get_post_meta($post->ID,'love',true)); } else { echo '0'; }?> <?php _e('Thumb' , 'grace'); ?></span>
				</div>
				<?php if(grace_option("is_shared")){
						require_once( get_template_directory() . '/inc/single/share.php'); 
					}
				?>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<article class="col-md-12 fluid pt-4">
				<div class="article article-content-img">
					<div class="content"><?php the_content(); ?></div>
					<div class="copyright mt-3 clearfix">
						<div class="tags float-left d-none d-md-block d-lg-block mt-3">
							<span class="fa fa-tags"></span>
							<?php if ( get_the_tags() ) { the_tags('', ' ', ''); } else{ echo '<a>' . __( 'None' , 'grace') . '</a>';  }?>
						</div>
						<div class="float-right mt-3">
							<span id="copyright"><?php _e('© The copyright belongs to the author','grace'); ?></span>
						</div>
					</div>
				</div>
				<?php require_once( get_template_directory() . '/inc/single/nav-next-link.php'); ?>
				<?php require_once( get_template_directory() . '/inc/author.php'); ?>
				<?php comments_template(); ?>
			</article>
		</div>
	</div>
<?php endif;?>
</main>
