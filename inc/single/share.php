<?php

?>

<div class="share-group pt-3 pt-md-4 d-none d-md-block d-lg-block">
	<a href="javascript:;" class="plain twitter" onclick="share('qq');" rel="nofollow">
		<div class="wrap">
			<i class="grace v3-qq"></i>
		</div>
	</a>
	<a href="javascript:;" class="plain weibo" onclick="share('weibo');" rel="nofollow">
		<div class="wrap">
			<i class="grace v3-weibo"></i>
		</div>
	</a>
	<a href="javascript:;" class="plain weixin pop" rel="nofollow">
		<div class="wrap">
			<i class="grace v3-weixin"></i>
		</div>
		<div class="share-int">
			<div class="qrcode" data-url="<?php the_permalink() ?>"></div>
			<p>微信“扫一扫”，点击右上角分享</p>
		</div>
	</a>
	<a href="javascript:;" class="plain qzone style-plain" onclick="share('qzone');" rel="nofollow">
		<div class="wrap">
			<i class="grace v3-qzone"></i>
		</div>
	</a>
	<a href="javascript:;" class="plain twitter style-plain" onclick="share('twitter');" rel="nofollow">
		<div class="wrap">
			<i class="grace v3-twitter"></i>
		</div>
	</a>
</div>
