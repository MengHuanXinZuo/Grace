<?php

?>

<div class="share-group-slider d-none d-md-block d-lg-block list-group">
	<a id="show_category_list" href="javascript:;" class="list-group-item" rel="nofollow">
		<div class="wrap">
			<i class="fa fa-navicon"></i>
		</div>
	</a>
	<a href="javascript:;" class="list-group-item" onclick="share('qq');" rel="nofollow">
		<div class="wrap">
			<i class="fa fa-qq" ></i>
		</div>
	</a>
	<a href="javascript:;" class="list-group-item" onclick="share('weibo');" rel="nofollow">
		<div class="wrap">
			<i class="fa fa-weibo" style="color: #EB192D;"></i>
		</div>
	</a>
	<a href="javascript:;" class="list-group-item" onclick="share_qrcode('<?php the_permalink(); ?>')" rel="nofollow">
		<div class="wrap">
			<i class="fa fa-qrcode"></i>
		</div>
	</a>
	<a href="javascript:;" class="list-group-item" onclick="share('qzone');" rel="nofollow">
		<div class="wrap">
			<i class="grace v3-qzone" style="color: #FFBD27;"></i>
		</div>
	</a>
	<a href="javascript:;" class="list-group-item" onclick="share('twitter');" rel="nofollow">
		<div class="wrap">
			<i class="fa fa-twitter"></i>
		</div>
	</a>
</div>

	<?php echo grace_get_article_category_list(); ?>
