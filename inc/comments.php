<?php

/**
 * Add the reply WeChat push
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
if (grace_option('single_comment_sc')) {
    add_action('comment_post', 'sc_send', 19, 2);
    function sc_send($comment_id) {
        $comment = get_comment($comment_id);
        $key = grace_option('single_comment_key');
        $postdata = http_build_query(  
            array(  
            'text' => '文章《' . $comment->post_title . '》有新的留言',  
            'desp' => '"' . get_comment_author($comment) . '"：' . $comment->comment_content,
            )
        );
        $opts = array('http' =>  
            array(
            'method'  => 'POST',  
            'header'  => 'Content-type: application/x-www-form-urlencoded',  
            'content' => $postdata  
            )  
        );
        $context = stream_context_create($opts);
        return $result = file_get_contents('https://sc.ftqq.com/'.$key.'.send', false, $context);  
    }
}

/**
 * Add @ for reply
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
add_filter( 'comment_text' , 'grace_comment_add_at', 20, 2);

function grace_comment_add_at( $comment_text, $comment = '') {
  if( $comment->comment_parent > 0) {
    $comment_text = '<span">@'.get_comment_author( $comment->comment_parent ) . '</span> ' . $comment_text;
  }
  return $comment_text;
}

/**
 * Include scripts files
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
function grace_comment_scripts(){
    wp_enqueue_script( 'comment', get_template_directory_uri() . '/static/js/comments.min.js' , array(), GRACE_VERSION);
    wp_localize_script( 'comment', 'ajaxcomment', array(
        'ajax_url'   => admin_url('admin-ajax.php'),
        'order' => get_option('comment_order'),
        'formpostion' => 'bottom',
    ) );
}

/**
 * Comment 500 error
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
function grace_comment_err($a) {
    header('HTTP/1.0 500 Internal Server Error');
    header('Content-Type: text/plain;charset=UTF-8');
    echo $a;
    exit;
}

/**
 * Comment ajax callback
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
function grace_comment_callback(){
    $comment = wp_handle_comment_submission( wp_unslash( $_POST ) );
    if ( is_wp_error( $comment ) ) {
        $data = $comment->get_error_data();
        if ( ! empty( $data ) ) {
        	grace_comment_err($comment->get_error_message());
        } else {
            exit;
        }
    }
    $user = wp_get_current_user();
    do_action('set_comment_cookies', $comment, $user);
    $GLOBALS['comment'] = $comment;
    ?>
    <li class="comment cleanfix" id="comment-<?php comment_ID(); ?>">
        <div class="avatar float-left d-inline-block mr-2">
            <?php if(function_exists('get_avatar') && get_option('show_avatars')) { echo get_avatar($comment, 50); } ?>
        </div>
        <div class="info clearfix">
            <div class="clearfix">
                <?php printf(__('<cite class="author_name">%s</cite>'), get_comment_author_link()); ?>
                <div class="content mb-2">
                    <?php comment_text(); ?>
                </div>
            </div>
            <div class="clearfix">
                <div class="meta clearfix">
                    <span class="date float-left"><?php echo get_comment_date(); ?></span>
                </div>  
            </div>
        </div>
    </li>
    <?php die();
}

add_action('wp_enqueue_scripts', 'grace_comment_scripts');
add_action('wp_ajax_nopriv_ajax_comment', 'grace_comment_callback');
add_action('wp_ajax_ajax_comment', 'grace_comment_callback');

function plc_comment_post( $incoming_comment ) {
	if ( !empty($_POST['spam_confirmed']) ) {
		if ( in_array( $incoming_comment['comment_type'], array('pingback', 'trackback') ) ) return $incoming_comment;
		if(grace_option("is_comment_check")){
			//方法一: 直接挡掉垃圾评论
			die();
		}else{
			//方法二: 标记为 spam, 留在资料库检查是否误判.
			add_filter('pre_comment_approved', create_function('', 'return "spam";'));
			$incoming_comment['comment_content'] = "[ 小墙判断这是 Spam! ]  -->>   ". $_POST['spam_confirmed'];
		}
	}
    $incoming_comment['comment_content'] = htmlspecialchars($incoming_comment['comment_content']); 
    $incoming_comment['comment_content'] = str_replace( "'", '&apos;', $incoming_comment['comment_content'] ); 
    return( $incoming_comment ); 
} 
function plc_comment_display( $comment_to_display ) { 
    $comment_to_display = str_replace( '&apos;', "'", $comment_to_display ); 
    return $comment_to_display; 
} 

function gate() {
	if ( !empty($_POST['comment_w']) && empty($_POST['comment']) ) {
		$_POST['comment'] = $_POST['comment_w'];
	} else {
		$request = $_SERVER['REQUEST_URI'];
		$referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '隐瞒';
		$IP      = isset($_SERVER["HTTP_X_FORWARDED_FOR"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] . '(透过代理)' : $_SERVER["REMOTE_ADDR"];
		$way     = isset($_POST['comment_w']) ? '手动操作': '未经评论表格，脚本代刷评论';
		$spamcom = isset($_POST['comment']) ? $_POST['comment'] : null;
		$_POST['spam_confirmed'] = "方式: ". $way. "\n內容: ". $spamcom. "\n";
	}
}

add_action('init', 'gate', 1);
add_filter( 'preprocess_comment', 'plc_comment_post', '', 1); 
add_filter( 'comment_text', 'plc_comment_display', '', 1); 
add_filter( 'comment_text_rss', 'plc_comment_display', '', 1); 
add_filter( 'comment_excerpt', 'plc_comment_display', '', 1);
