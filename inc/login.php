<?php
	
session_start();

//添加自定义CSS，用于修改登录页面样式，可以先打开默认的登录界面，进入页面调试，通过自定义样式来覆盖默认的样式
function custom_login() {
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/static/css/login_style.css?ver=' . GRACE_VERSION ;?>" />
    <script type="text/javascript" src="<?php echo get_template_directory_uri() . '/static/js/jquery.min.js?ver=' . GRACE_VERSION ;?>"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() . '/inc/Geetest/static/gt.js?ver=' . GRACE_VERSION ;?>"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() . '/static/js/login.js?ver=' . GRACE_VERSION ;?>"></script>
    <script type="text/javascript">
    	var handlerEmbed = function (captchaObj) {
	    $("#wp-submit").click(function (e) {
	        var validate = captchaObj.getValidate();
	            if (!validate) {
	                $("#notice")[0].className = "show";
	                setTimeout(function () {
	                    $("#notice")[0].className = "hide";
	                }, 2000);
	                e.preventDefault();
	            }
	    });
	    captchaObj.appendTo("#embed-captcha");
	    captchaObj.onReady(function () {
	        $("#wait")[0].className = "hide";
	    });
	};

		$.ajax({
	    url: "<?php echo get_template_directory_uri() . '/inc/Geetest/web/StartCaptchaServlet.php'; ?>?t=" + (new Date()).getTime(), // 加随机数防止缓存
	    type: "get",
	    dataType: "json",
	    success: function (data) {
	        initGeetest({
	        	width: '100%',
	        	https: true,
	            gt: data.gt,
	            challenge: data.challenge,
	            new_captcha: data.new_captcha,
	            product: "popup", // 产品形式，包括：float，embed，popup。注意只对PC版验证码有效
	            offline: !data.success 
	        }, handlerEmbed);
	    }
	});
	</script>
    <?php
}
add_action('login_head', 'custom_login');

//插入自定义的Logo和背景图片，其实就是一个css样式，下面的写法是为了方便在主题设置里面添加图片链接
function custom_login_logo() {
	$default_logo = get_template_directory_uri() . "/static/images/Logo.png";
	$default_background = get_template_directory_uri() . "/static/images/background_2.jpg";
  ?>
    <style type="text/css">
        .login h1 a {
            width: 320px;
            height: 60px;
            background:url( <?php echo (grace_option("site_logo") ? grace_option("site_logo") : $default_logo); ?> ) no-repeat center top;
        }
        body.login{
            background:url(<?php echo (grace_option("site_login") ? grace_option("site_login") : $default_background ); ?>) 0 0 no-repeat;
        }
    </style>    
    <?php 
}
add_action('login_head', 'custom_login_logo');

//自定义登录界面Logo点击链接
add_filter('login_headerurl', create_function(false,"return get_bloginfo('url');"));

//自定义登录页面的LOGO提示为网站名称
add_filter('login_headertitle', create_function(false,"return get_bloginfo('name');"));

//自定义登录和注册页面底部信息
function custom_html() {
    echo '<p class="login_footer"> © 欢迎来到 '. get_bloginfo(name).' </p>';
}
add_action('login_footer', 'custom_html');

//在登录框添加验证码
function custom_login_message() {
?>
	<?php if(grace_option('is_login_check')){ ?>
		<?php if($_SESSION['gtserver'] == 1){ ?>
		    <div id="embed-captcha"></div>
		    <p id="wait" class="show">正在加载验证码......</p>
		    <p id="notice" class="hide">请先完成验证</p>
	    <?php } 
    }?>
    <input id="check_code" class="check_code" value="" type="text" name="check_code" />
    <p class="forget_password">
        <a href="<?php echo wp_lostpassword_url( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ); ?>" title="忘记密码">忘记密码？</a>
    </p>
<?php
}
add_action('login_form', 'custom_login_message');

//登录时进行输入验证码
function check_extra_login_fields($user, $username='', $password='') {
	require_once( get_template_directory() . '/inc/Geetest/web/VerifyLoginServlet.php');
	if(grace_option('is_login_check')){
	    if ( !$_SESSION['gtserver_result'] && $_SESSION['gtserver']) {
	        $error = new WP_Error();
	        $error->add( 'not_human', "<strong>错误</strong>: 验证码错误！" );
	        return $error;
	    }
   	}
    
    if($_POST['check_code'] !== ""){
        $error = new WP_Error();
        $error->add( 'not_human', "<strong>错误</strong>: 机器人不能登录！" );
        return $error;
    }
    else{
        return $user;
    }
}
add_filter( 'wp_authenticate_user', 'check_extra_login_fields', 10, 2 );

/*注册时添加验证码*/
add_action( 'register_form', 'show_extra_register_fields' );
function show_extra_register_fields(){
?>
    <p>
        <label for="password">密码<br/>
            <input id="password" class="input" type="password" tabindex="30" size="25" value="" name="password" />
        </label>
    </p>
    <p>
        <label for="repeat_password">请重复密码<br/>
            <input id="repeat_password" class="input" type="password" tabindex="40" size="25" value="" name="repeat_password" />
        </label>
    </p>
    <?php if(grace_option('is_register_check')){ ?>
	    <?php if($_SESSION['gtserver']){ ?>
	    <div id="embed-captcha"></div>
	    <p id="wait" class="show">正在加载验证码......</p>
	    <p id="notice" class="hide">请先完成验证</p>
	    <?php }else{?>
	    <p>
	        <label for="check_human_2">请输入验证码：<br/>
	        	<input id="check_human_2" class="input" type="text" tabindex="40" size="25" value="" name="check_human_2" />
	        	<img id="check_img" title="点击刷新" src="<?php echo get_template_directory_uri(); ?>/inc/ValidateCode/CheckCode.php" align="absbottom" onclick="this.src='<?php echo get_template_directory_uri(); ?>/inc/ValidateCode/CheckCode.php?'+Math.random();"></img>
	        	
	        	<a href="#" onclick="refresh_check()">看不清？换一张</a>
	        </label>
	    </p>
	    <?php }
	}?>
    <input id="check_code" class="check_code" type="text" value="" name="check_code" />
<?php
}

//检查两次密码是否输入一致，以及检查验证码是否正确
add_action( 'register_post', 'check_extra_register_fields', 10, 3 );
function check_extra_register_fields($login, $email, $errors) {
	require_once( get_template_directory() . '/inc/Geetest/web/VerifyLoginServlet.php');
	
    if ( $_POST['password'] !== $_POST['repeat_password'] ) {
        $errors->add( 'passwords_not_matched', "<strong>错误</strong>：两次密码不一致" );
        return $error;
    }
    
    if(grace_option('is_register_check')){
	    if($_SESSION['gtserver']){
		    if ( !$_SESSION['gtserver_result'] ) {
		        $errors->add( 'not_human', "<strong>错误</strong>：验证码错误！" );
		        return $error;
		    }
	    }else{
	    	if($_POST['check_human_2'] !== $_SESSION['authnum_session']){
	    		$errors->add( 'not_human', "<strong>错误</strong>：验证码错误！" );
		        return $error;
	    	}
	    }
    }
    if($_POST['check_code'] !== ""){        
        $error->add( 'not_human', "<strong>错误</strong>: 机器人不能注册！" );
        return $error;
    }
}
//存储用户输入的密码，如果用户不输入密码，由系统自动生成，并邮件通知用户
add_action( 'user_register', 'register_extra_fields', 100 );
function register_extra_fields( $user_id ){
    $userdata = array();
 
    $userdata['ID'] = $user_id;
    if ( $_POST['password'] !== '' ) {
        $userdata['user_pass'] = $_POST['password'];
    }
    $new_user_id = wp_update_user( $userdata );
}
//修改注册时的提示信息
add_filter( 'gettext', 'edit_password_email_text' );
function edit_password_email_text ( $text ) {
    if ( $text == '注册确认信将会被寄给您。' ) {
        $text = '如果您没有输入密码，系统会自动产生密码，并通过电子邮件通知您，请注意查收。';
    }
    return $text;
}

?>