<?php

/**
 * About author for single
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
?>
<div class="author mt-3 clearfix">
	<div class="meta float-md-left mb-2">
		<?php echo get_avatar( get_the_author_meta( 'user_email' ) ,56 ); ?>
		<p class="name">
			<?php the_author_posts_link(); ?>
		</p>
		<p class="motto"><?php if (get_the_author_meta('description')) { echo strip_tags(get_the_author_meta('description'));} else {_e('The person is so lazy that he left nothing.','grace');} ?></p>
	</div>
	<div class="share float-md-right text-center">
		<?php if ( grace_option( 'contact_qq' ) ) { 								
			$qq_qr = get_the_author_meta('qq_qr') ? get_the_author_meta('qq_qr') : grace_option('default_qr');
			?>
	   		<a href="javascript:;" title="QQ联系" role="button" class="btn btn-qq mr-2" onclick="contactView('<?php echo $qq_qr; ?>')"><span class="fa fa-qq"></span></a>	
	   	<?php } ?>
	   	<?php if ( grace_option( 'contact_wechat' ) ) { 								
			$wechat_qr = get_the_author_meta('wechat_qr') ? get_the_author_meta('wechat_qr') : grace_option('default_qr');
			?>
	   		<a href="javascript:;" title="微信联系" role="button" class="btn btn-wechat mr-2" onclick="contactView('<?php echo $wechat_qr; ?>')"><span class="fa fa-wechat"></span></a>	
	   	<?php } ?>
		<?php if (grace_option('donate_status')) { 
			$author_aipay = get_the_author_meta('alipay_qr') ? get_the_author_meta('alipay_qr') : grace_option('default_qr');
    		$author_wechatpay = get_the_author_meta('wechatpay_qr') ? get_the_author_meta('wechatpay_qr') : grace_option('default_qr');
    	?>
		<a href="javascript:;" id="donate" class="btn btn-donate mr-2" role="button" onclick="donateView('<?php echo $author_aipay ?>','<?php echo $author_wechatpay ?>')" title="<?php _e('Donate','grace')?>"><i class="fa fa-dollar"></i></a>
		<?php } ?>
		<a href="javascript:;" id="thumbs" data-action="love" data-id="<?php the_ID(); ?>" role="button" class="btn btn-thumbs <?php if(isset($_COOKIE['love_'.$post->ID])) echo 'done';?>" title="<?php _e('Thumbs','grace')?>"><i class="grace v3-thumbs"></i></a>
	</div>
</div>