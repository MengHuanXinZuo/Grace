<?php

?>

<div class="col-md-6 col-lg-4">
	<div class="sorta sortb my-3 my-lg-4" style="background-image:url(<?php echo grace_thumbnail_url(); ?>);">
		<a href="<?php the_permalink(); ?>" class="desc text-dark">
			<span><?php foreach((get_the_category()) as $category) { echo $category->cat_name; } ?></span>
			<h3 class="mt-2"><?php the_title(); ?></h3>
		</a>
	</div>
</div>