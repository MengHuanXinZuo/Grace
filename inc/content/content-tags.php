<?php

?>

<article class="sortb col-lg-4 col-md-6 category-item">
	<div class="alpha-item globe-block">
		<a href="<?php the_permalink() ?>"><img class="thumbnail" src="<?php echo grace_thumbnail_url(); ?>" alt=""></a>
	
		<div class="meta">
			<h3><a href="<?php the_permalink(); ?>" class="text-dark"><?php the_title(); ?></a></h3>
			<span class="category"><?php foreach((get_the_category()) as $category) {?>
				<a style="color: #FFFFFF;" href="<?php echo get_category_link($category->cat_ID);?>"><?php echo $category->cat_name; ?></a> 
				<?php } ?>
			</span>
			<p><?php echo wp_trim_words(get_the_excerpt(),150); ?></p>
		</div> 
	</div>
</article>