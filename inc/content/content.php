<?php

/**
 * The template for content
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
?>
<article class="gamma-item globe-block">
	<div class="clearfix">
		<div class="gamma-item-body p-0">
			<div class="thumb m-3">
				<?php grace_thumbnail(); ?>
			</div>
			<div class="gamma-item-header">
				<header class="header">
					<a class="label" href="<?php $category = get_the_category();echo get_category_link($category[0] -> term_id) . '">' . $category[0] -> cat_name ; ?><i class="label-arrow"></i></a>
					<h2 class="title text-center">
						<a class="text-dark" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</h2>
				</header>
				<div class="summary text-secondary">
					<p><?php echo wp_trim_words(get_the_excerpt(),150); ?></p>
				</div>
			</div>
		</div>
		<div class="metas">
			<span class="d-none d-md-block d-lg-block float-left">
				<a class="mr-2" href="<?php echo get_month_link( get_the_date('Y'), get_the_date('m') ); ?>"><i class="fa fa-calendar"></i> <?php echo get_the_date(); ?></a>
				<a class="mr-2" href="<?php the_permalink() ?>#respond"><i class="fa fa-commenting-o"></i> <?php comments_number('0', '1', '%'); ?>&nbsp;条评论</a>
			</span>
			<span class="float-left">
				<a class="mr-2" href="<?php the_permalink() ?>"><i class="fa fa-eye"></i> <?php echo grace_get_post_views(); ?>&nbsp;次阅读</a>
				<a class="mr-2" href="<?php the_permalink() ?>"><i class="fa fa-thumbs-o-up"></i> <?php if( get_post_meta($post->ID,'love',true) ){ echo get_post_meta($post->ID,'love',true); } else { echo '0'; }?>&nbsp;人点赞</a>
			</span>
			<span class="float-right">
				<a class="read-more" href="<?php the_permalink() ?>" title="阅读全文">阅读全文 <i class="fa fa-chevron-circle-right"></i></a>
			</span>
		</div>
	</div>
</article>