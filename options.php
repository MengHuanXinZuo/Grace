<?php

/**
 * The template for optionsframework option page
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
function optionsframework_option_name() {
	$themename = wp_get_theme();
	$themename = preg_replace("/\W/", "_", strtolower($themename) );
	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

function optionsframework_options() {

	$imagepath =  get_template_directory_uri() . '/inc/options/images/';

	$options = array();
	
	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories('hide_empty=0');
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}

	$options[] = array(
		'name' => __( 'Global Config' , 'grace' ),
		'type' => 'heading');

	$options[] = array(
		'name' => __( 'Function status' , 'grace' ),
		'desc' => __( 'Whether to enable the web compression?' , 'grace' ),
		'id' => 'global_html',
		'std' => '0',
		'type' => 'checkbox');

	$options[] = array(
		'name' => __( 'Favicon ico' , 'grace' ),
		'id' => 'global_ico',
		'type' => 'upload'
	);

	$options[] = array(
		'name' => __( 'Default gravatar' , 'grace' ),
		'id' => 'image_default_gravatar',
		'std' => get_template_directory_uri() . '/static/images/default/gravatar.png',
		'type' => 'upload'
	);

	$options[] = array(
		'name' => __( 'Login page url' , 'grace' ),
		'id' => 'global_login',
		'type' => 'text'
	);	

	$options[] = array(
		'name' => __( 'ICP number' , 'grace' ),
		'id' => 'footer_icp_num',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'GA number' , 'grace' ),
		'id' => 'footer_gov_num',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'GA link' , 'grace' ),
		'id' => 'footer_gov_link',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Index Config' , 'grace' ),
		'type' => 'heading');

	$options[] = array(
		'name' => __( 'Function status' , 'grace' ),
		'desc' => __( 'Whether to enable the index page?' , 'grace' ),
		'id' => 'index_status',
		'std' => '0',
		'type' => 'checkbox');

	$options[] = array(
		'name' => __( 'Project name' , 'grace' ),
		'id' => 'index_project_1',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'Project meta' , 'grace' ),
		'id' => 'index_meta_1',
		'type' => 'textarea');

	$options[] = array(
		'name' => __( 'Project single' , 'grace' ),
		'id' => 'index_single_1',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'Project name' , 'grace' ),
		'id' => 'index_project_2',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'Project meta' , 'grace' ),
		'id' => 'index_meta_2',
		'type' => 'textarea');

	$options[] = array(
		'name' => __( 'Project category' , 'grace' ),
		'id' => 'index_category_2',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'Project name' , 'grace' ),
		'id' => 'index_project_3',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'Project meta' , 'grace' ),
		'id' => 'index_meta_3',
		'type' => 'textarea');

	$options[] = array(
		'name' => __( 'Project category' , 'grace' ),
		'id' => 'index_category_3',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'Image Config' , 'grace' ),
		'type' => 'heading');

	$options[] = array(
		'name' => __( 'Category banner' , 'grace' ),
		'id' => 'image_default_category',
		'std' => get_template_directory_uri() . '/static/images/default/banner.png',
		'type' => 'upload'
	);

	$options[] = array(
		'name' => __( 'Tag banner' , 'grace' ),
		'id' => 'image_default_tag',
		'std' => get_template_directory_uri() . '/static/images/default/banner.png',
		'type' => 'upload'
	);

	$options[] = array(
		'name' => __( 'Search banner' , 'grace' ),
		'id' => 'image_default_search',
		'std' => get_template_directory_uri() . '/static/images/default/banner.png',
		'type' => 'upload'
	);

	$options[] = array(
		'name' => __( 'Default share images' , 'grace' ),
		'id' => 'image_default_share',
		'std' => get_template_directory_uri() . '/static/images/default/thumbnail.png',
		'type' => 'upload'
	);

	$options[] = array(
		'name' => __( 'Default author images' , 'grace' ),
		'id' => 'image_default_author',
		'std' => get_template_directory_uri() . '/static/images/default/author.png',
		'type' => 'upload'
	);

	$options[] = array(
		'name' => __( '404 images' , 'grace' ),
		'id' => 'image_default_404',
		'std' => get_template_directory_uri() . '/static/images/default/404.png',
		'type' => 'upload'
	);

	$options[] = array(
		'name' => __( 'Single Config' , 'grace' ),
		'type' => 'heading');
		
	$options[] = array(
		'name' => '文章页面图片弹出方式',
		'id' => "article_img_type",
		'std' => "page",
		'type' => "select",
		'class' => 'mini',
		'options' => array(
			'page' => '弹框',
			'photos' => '相册',
	));	
	
	$options[] = array(
		'name' => '是否启用文章分享',
		'desc' => '在文章页面左侧显示分享栏以及文章内容目录',
		'id' => 'is_shared',
		'std' => '1',
		'type' => 'checkbox');
		
	$options[] = array(
		'name' => '直接丢弃垃圾评论',
		'desc' => '若不直接丢弃垃圾评论则保留在数据库中以备查',
		'id' => 'is_comment_check',
		'std' => '1',
		'type' => 'checkbox');

	$options[] = array(
		'name' => __( 'Single layout' , 'grace' ),
		'id' => "single_layout",
		'std' => "alpha",
		'type' => "select",
		'class' => 'mini',
		'options' => array(
			'alpha' => __( 'Style 1', 'grace' ),
			'gamma' => __( 'Style 2', 'grace' ),
	));	
	
	$options[] = array(
		'name' => '选择分类目录',
		'desc' => '为特定分类目录选择文章显示样式',
		'id' => "custom_category",
		'std' => 0,
		'class' => 'mini d-inline-block',
		'type' => 'select',
		'options' => $options_categories
	);
	
	$options[] = array(
		'name' => '选择文章样式（针对选定分类目录）',
		'id' => "single_layout_category",
		'desc' => '为特定分类目录选择文章显示样式',
		'std' => "alpha",
		'type' => "select",
		'class' => 'mini d-inline-block',
		'options' => array(
			'alpha' => __( 'Style 1', 'grace' ),
			'gamma' => __( 'Style 2', 'grace' ),
	));	

	$options[] = array(
		'name' => __( 'WeChat push' , 'grace' ),
		'desc' => __( 'Whether to enable Server Chan?' , 'grace' ),
		'id' => 'single_comment_sc',
		'std' => '0',
		'type' => 'checkbox');

	$options[] = array(
		'name' => __( 'SCKEY' , 'grace' ),
		'id' => 'single_comment_key',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'Index thumbnail' , 'grace' ),
		'id' => 'image_thumbnail_index',
		'std' => get_template_directory_uri() . '/static/images/default/thumbnail.png',
		'type' => 'upload'
	);

	$options[] = array(
		'name' => __( 'Inside thumbnail' , 'grace' ),
		'id' => 'image_thumbnail_inside',
		'std' => get_template_directory_uri() . '/static/images/default/thumbnail.png',
		'type' => 'upload'
	);

	$options[] = array(
		'name' => __( 'Copyright content' , 'grace' ),
		'id' => 'single_copyright',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'Page Config' , 'grace' ),
		'type' => 'heading');
			
	$options[] = array(
		'name' => __( 'Page permalink' , 'grace' ),
		'desc' => __( 'Whether to enable Page permalink?' , 'grace' ),
		'id' => 'page_html',
		'std' => '0',
		'type' => 'checkbox');

	$options[] = array(
		'name' => __( 'Mail Config' , 'grace' ),
		'type' => 'heading');

	$options[] = array(
		'name' => __( 'SMTP service' , 'grace' ),
		'desc' => __( 'Whether to enable SMTP service?' , 'grace' ),
		'id' => 'mail_smtps',
		'std' => '0',
		'type' => 'checkbox');

	$options[] = array(
		'name' => __( 'Sender' , 'grace' ),
		'id' => 'mail_name',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'Mail server' , 'grace' ),
		'id' => 'mail_host',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'Server port' , 'grace' ),
		'id' => 'mail_port',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'Mail account' , 'grace' ),
		'id' => 'mail_username',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'Email password' , 'grace' ),
		'id' => 'mail_passwd',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'Enable SMTPAuth service' , 'grace' ),
		'desc' => __( 'Whether to enable SMTPAuth service?' , 'grace' ),
		'id' => 'mail_smtpauth',
		'std' => '1',
		'type' => 'checkbox');

	$options[] = array(
		'name' => __( 'SMTPSecure set' , 'grace' ),
		'id' => 'mail_smtpsecure',
		'std' => 'ssl',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'Carousel Config' , 'grace' ),
		'type' => 'heading');

	$options[] = array(
		'name' => __( 'Function status' , 'grace' ),
		'desc' => __( 'Whether to enable the carousel?' , 'grace' ),
		'id' => 'carousel_status',
		'std' => '0',
		'type' => 'checkbox');

	$options[] = array(
		'name' => __( 'The first image' , 'grace' ),
		'id' => 'carousel_img_1',
		'type' => 'upload');

	$options[] = array(
		'name' => __( 'The first url' , 'grace' ),
		'id' => 'carousel_url_1',
		'std' => '',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'The first title' , 'grace' ),
		'id' => 'carousel_title_1',
		'std' => '',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'The first meta' , 'grace' ),
		'id' => 'carousel_meta_1',
		'std' => '',
		'type' => 'textarea');

	$options[] = array(
		'name' => __( 'The second image' , 'grace' ),
		'id' => 'carousel_img_2',
		'type' => 'upload');

	$options[] = array(
		'name' => __( 'The second url' , 'grace' ),
		'id' => 'carousel_url_2',
		'std' => '',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'The second title' , 'grace' ),
		'id' => 'carousel_title_2',
		'std' => '',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'The second meta' , 'grace' ),
		'id' => 'carousel_meta_2',
		'std' => '',
		'type' => 'textarea');

	$options[] = array(
		'name' => __( 'The third image' , 'grace' ),
		'id' => 'carousel_img_3',
		'type' => 'upload');

	$options[] = array(
		'name' => __( 'The third url' , 'grace' ),
		'id' => 'carousel_url_3',
		'std' => '',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'The third title' , 'grace' ),
		'id' => 'carousel_title_3',
		'std' => '',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'The third meta' , 'grace' ),
		'id' => 'carousel_meta_3',
		'std' => '',
		'type' => 'textarea');

	$options[] = array(
		'name' => __( 'The fourth image' , 'grace' ),
		'id' => 'carousel_img_4',
		'type' => 'upload');

	$options[] = array(
		'name' => __( 'The fourth url' , 'grace' ),
		'id' => 'carousel_url_4',
		'std' => '',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'The fourth title' , 'grace' ),
		'id' => 'carousel_title_4',
		'std' => '',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'The fourth meta' , 'grace' ),
		'id' => 'carousel_meta_4',
		'std' => '',
		'type' => 'textarea');

	$options[] = array(
		'name' => __( 'The fifth image' , 'grace' ),
		'id' => 'carousel_img_5',
		'type' => 'upload');

	$options[] = array(
		'name' => __( 'The fifth url' , 'grace' ),
		'id' => 'carousel_url_5',
		'std' => '',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'The fifth title' , 'grace' ),
		'id' => 'carousel_title_5',
		'std' => '',
		'type' => 'text');

	$options[] = array(
		'name' => __( 'The fifth meta' , 'grace' ),
		'id' => 'carousel_meta_5',
		'std' => '',
		'type' => 'textarea');

	$options[] = array(
		'name' => '二维码设置',
		'type' => 'heading');

	$options[] = array(
		'name' => __( 'Function status' , 'grace' ),
		'desc' => __( 'Whether to enable the donate?' , 'grace' ),
		'id' => 'donate_status',
		'std' => '0',
		'type' => 'checkbox');
		
	$options[] = array(
		'name' => 'QQ联系',
		'desc' => '展示QQ联系二维码',
		'id' => 'contact_qq',
		'std' => '0',
		'type' => 'checkbox');
		
	$options[] = array(
		'name' => '微信联系',
		'desc' => '展示微信联系二维码',
		'id' => 'contact_wechat',
		'std' => '0',
		'type' => 'checkbox');

	$options[] = array(
		'name' => '默认二维码',
		'id' => 'default_qr',
		'std' => get_template_directory_uri() . '/static/images/default/qr.png',
		'type' => 'upload');
		
	$options[] = array(
		'name' => '登录页面',
		'type' => 'heading');
		
	$options[] = array(
		'name' => '是否启用登录验证码',
		'desc' => '登录时启用极验验证',
		'id' => 'is_login_check',
		'std' => '1',
		'type' => 'checkbox');
		
	$options[] = array(
		'name' => '是否启用注册验证码',
		'desc' => '注册时启用极验验证，验证码服务器宕机时采用自有的图片验证码',
		'id' => 'is_register_check',
		'std' => '1',
		'type' => 'checkbox');
		
	$options[] = array(
		'name' => '站点Logo',
		'desc' => '不添加显示文字标题，推荐图片尺寸 175px*40px，保存成功则自动显示Logo图片',
		'id' => 'site_logo',
		'std' => get_template_directory_uri() . '/static/images/Logo.png',
		'type' => 'upload');
		
	$options[] = array(
		'name' => '登录界面背景',
		'desc' => '推荐图片为jpg',
		'id' => 'site_login',
		'std' => get_template_directory_uri() . '/static/images/background_2.jpg',
		'type' => 'upload');	
	
	$options[] = array(
		'name' => '站点SEO设置',
		'type' => 'heading');

	$options[] = array(
		'name' => __( 'Keywords' , 'grace' ),
		'id' => 'global_keywords',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Description' , 'grace' ),
		'id' => 'global_description',
		'type' => 'textarea'
	);
		
	$options[] = array(
		'name' => '站点统计',
		'id' => 'site_tongji',
		'std' => '',
		'type' => 'textarea');
		
	$options[] = array(
		'name' => '网站公告',
		'type' => 'heading');
	$options[] = array(
		'name' => '是否启用首页网站公告栏',
		'desc' => '在网站首页标题栏下显示公告内容',
		'id' => 'is_notice',
		'std' => '0',
		'type' => 'checkbox');
	$options[] = array(
		'name' => '公告链接',
		'desc' => '网站公告内容的链接',
		'id' => 'notice_links',
		'type' => 'text');
	$options[] = array(
		'name' => '链接打开方式',
		'desc' => '公告链接的打开方式',
		'id' => 'notice_mode',
		'std' => '_blank',
		'type' => 'select',
		'class' => 'mini',
		'options' => array(
			'_blank' => '新窗口或新标签。',
			'_top' => '不包含框架的当前窗口或标签。',
			'_none' => '同一窗口或标签。'));
	$options[] = array(
		'name' => '公告内容',
		'id' => 'notice_content',
		'std' => '',
		'type' => 'textarea');
		
	$options[] = array(
		'name' => '用户中心',
		'type' => 'heading');

	$options[] = array(
		'name' => '每页显示用户数',
		'id' => 'users_number',
		'std' => '10',
		'type' => 'text'
	);
	
	$options[] = array(
		'name' => '用户列表标题',
		'id' => 'users_title',
		'type' => 'text'
	);

	$options[] = array(
		'name' => '用户列表描述',
		'id' => 'users_description',
		'type' => 'textarea'
	);
	
	return $options;
}