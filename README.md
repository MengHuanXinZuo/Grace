<p align="center">
<img src="https://camo.githubusercontent.com/9da13920699b57a32ac7a6cf5debec0b1efc7e1e/68747470733a2f2f73322e617831782e636f6d2f323032302f30322f31362f33396e4734532e706e67">
</p>

<p align="center">
<img src="https://img.shields.io/badge/php-%3E%3D7.0.0-blue">
<img src="https://img.shields.io/badge/wordpress-v5.2.3%20tested-%234c1">
<a href="https://crowdin.com/project/kratos"><img src="https://badges.crowdin.net/kratos/localized.svg"></a>
<img src="https://img.shields.io/github/license/Vtrois/Kratos?color=%234c1">
</p>

## About

Kratos is a responsive WordPress theme focused on the user reading experience, just for fun 🎉

## Install

1. First download the theme's .zip file from the source file to your local computer.

2. From your WordPress Administration area, head to Appearance > Themes and click `Add New`.

3. The Add New theme screen has a new option, `Upload Theme`.

4. The theme upload form is now open, click `Choose File`, select the theme zip file on your computer and click `Install Now`.

5. The theme can now be activated from the administrator. Select the `Appearance` tab, then open the theme catalog, find the theme, and click the `Activate link`.

## Structure
Within the download you'll find the following directories and files. You'll see something like this 👇

```
Kratos
├── 404.php
├── LICENSE
├── README.md
├── assets
│   ├── css
│   │   ├── bootstrap.min.css
│   │   ├── iconfont.min.css
│   │   ├── kratos.css
│   │   ├── kratos.min.css
│   │   ├── layer.min.css
│   │   └── widget.min.css
│   ├── fonts
│   │   ├── iconfont.eot
│   │   ├── iconfont.svg
│   │   ├── iconfont.ttf
│   │   ├── iconfont.woff
│   │   └── iconfont.woff2
│   ├── img
│   │   ├── 404.jpg
│   │   ├── 404.svg
│   │   ├── about-background.png
│   │   ├── ad.png
│   │   ├── background.png
│   │   ├── default.jpg
│   │   ├── donate.png
│   │   ├── gravatar.png
│   │   ├── layer
│   │   │   ├── icon-ext.png
│   │   │   └── icon.png
│   │   ├── nothing.svg
│   │   ├── payment
│   │   │   ├── alipay.png
│   │   │   └── wechat.png
│   │   ├── police-ico.png
│   │   ├── smilies(has some emoji pic)
│   │   └── wechat.png
│   └── js
│       ├── bootstrap.min.js
│       ├── buttons
│       │   ├── images(has some button pic)
│       │   └── more.js
│       ├── comments.min.js
│       ├── jquery.min.js
│       ├── kratos.js
│       ├── kratos.min.js
│       ├── layer.min.js
│       └── widget.min.js
├── comments.php
├── custom
│   ├── custom.css
│   ├── custom.js
│   └── custom.php
├── footer.php
├── functions.php
├── header.php
├── inc
│   ├── options-framework
│   │   ├── autoload.php
│   │   ├── css
│   │   │   └── optionsframework.css
│   │   ├── images(has some options pic)
│   │   ├── includes
│   │   │   ├── class-options-framework-admin.php
│   │   │   ├── class-options-framework.php
│   │   │   ├── class-options-interface.php
│   │   │   ├── class-options-media-uploader.php
│   │   │   └── class-options-sanitization.php
│   │   └── js
│   │       ├── media-uploader.js
│   │       └── options-custom.js
│   ├── theme-article.php
│   ├── theme-core.php
│   ├── theme-navwalker.php
│   ├── theme-options.php
│   ├── theme-setting.php
│   ├── theme-shortcode.php
│   ├── theme-smtp.php
│   ├── theme-widgets.php
│   └── update-checker
│       ├── Puc
│       │   ├── v4
│       │   │   └── Factory.php
│       │   └── v4p9
│       │       ├── Autoloader.php
│       │       ├── DebugBar
│       │       │   ├── Extension.php
│       │       │   ├── Panel.php
│       │       │   ├── PluginExtension.php
│       │       │   ├── PluginPanel.php
│       │       │   └── ThemePanel.php
│       │       ├── Factory.php
│       │       ├── InstalledPackage.php
│       │       ├── Metadata.php
│       │       ├── OAuthSignature.php
│       │       ├── Plugin
│       │       │   ├── Info.php
│       │       │   ├── Package.php
│       │       │   ├── Ui.php
│       │       │   ├── Update.php
│       │       │   └── UpdateChecker.php
│       │       ├── Scheduler.php
│       │       ├── StateStore.php
│       │       ├── Theme
│       │       │   ├── Package.php
│       │       │   ├── Update.php
│       │       │   └── UpdateChecker.php
│       │       ├── Update.php
│       │       ├── UpdateChecker.php
│       │       ├── UpgraderStatus.php
│       │       ├── Utils.php
│       │       └── Vcs
│       │           ├── Api.php
│       │           ├── BaseChecker.php
│       │           ├── BitBucketApi.php
│       │           ├── GitHubApi.php
│       │           ├── GitLabApi.php
│       │           ├── PluginUpdateChecker.php
│       │           ├── Reference.php
│       │           └── ThemeUpdateChecker.php
│       ├── autoload.php
│       ├── css
│       │   └── puc-debug-bar.css
│       ├── js
│       │   └── debug-bar.js
│       └── vendor
│           ├── Parsedown.php
│           ├── ParsedownLegacy.php
│           ├── ParsedownModern.php
│           └── PucReadmeParser.php
├── index.php
├── languages
│   └── kratos.pot
├── page.php
├── pages
│   ├── page-content.php
│   ├── page-smilies.php
│   └── page-toolbar.php
├── screenshot.png
├── single.php
└── style.css
```

## Changelog
Detailed changes for each release are documented in the [release notes](https://github.com/Vtrois/Kratos/releases).

## Donation
If you find Kratos useful, you can buy us a cup of coffee

<p align="center">
<img width="650" src="https://camo.githubusercontent.com/45c296e69d4aa7275d4f7c8971afd8bb8011209f/68747470733a2f2f73322e617831782e636f6d2f323032302f30322f31362f3339314e67552e706e67">
</p>

## License

The code is available under the [MIT](https://github.com/Vtrois/Kratos/blob/master/LICENSE) license.

The document is licensed under a [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](http://creativecommons.org/licenses/by-nc-nd/4.0/).# Grace

#### 项目介绍
Grace主题，本主题是基于开源主题Dobby的基础上进行开发的，原作者主题地址：<a href="https://www.vtrois.com/theme-dobby.html">https://www.vtrois.com/theme-dobby.html</a>

#### 主题Demo示例
<a href="https://demo.vcblog.top">前往Demo</a>

#### 软件架构
软件架构说明


#### 安装教程

1. 可以直接把本项目克隆到WordPress主题目录下，wp-content\themes\
2. 也可以直接下载压缩包，在后台进行主题上传安装。

#### 使用说明

1. 本主题遵守开源协议GPL3.0。

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)