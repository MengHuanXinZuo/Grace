<?php

/**
 * The template for default page
 * Template Name: 页面模板-无侧边栏
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
get_header(); ?>
<main class="main alpha-content bg-light pb-4">
	<div class="container pt-4">
		<div class="row">
			<article class="col-md-12">
				<div class="article article-content-img">
					<?php if (have_posts()) : the_post(); update_post_caches($posts); ?>
					<div class="text-center text-dark">
						<h1><?php the_title(); ?></h1>
						<div class="about pt-md-2 mb-3">
							<span class="d-inline-block"><i class="grace v3-activity"></i> <?php echo get_the_date(); ?></span>
							<span class="d-none d-md-inline-block"><i class="grace v3-interactive"></i> <?php comments_number('0', '1', '%'); ?>&nbsp;<?php _e('Comments' , 'grace'); ?></span>
							<span class="d-inline-block"><i class="grace v3-browse"></i> <?php echo grace_get_post_views(); ?>&nbsp;<?php _e('Views' , 'grace'); ?></span>
							<span class="d-inline-block"><i class="grace v3-praise"></i> <?php if( get_post_meta($post->ID,'love',true) ){ echo num2tring(get_post_meta($post->ID,'love',true)); } else { echo '0'; }?>&nbsp;<?php _e('Thumb' , 'grace'); ?></span>
						</div>
					</div>
					<div class="content"><?php the_content(); ?></div>
					<?php endif; ?>
				</div>
				<?php comments_template(); ?>
			</article>
		</div>
	</div>
</main>
<?php get_footer(); ?>