<?php

/**
 * The template for users page
 * Template Name: 用户列表
 *
 * @author Vtrois <seaton@vtrois.com>
 * @license GPL-3.0
 * @since 1.0
 */
get_header(); ?>
<main class="main bg-light pb-4">
	<div class="container-fluid">
		<div class="row">
			<div class="banner-panel text-center" style="background-image: url(<?php echo grace_option('image_default_category'); ?>);">
				<h1 class="wow bounceInLeft"><?php echo grace_option('users_title') ? grace_option('users_title') : '作者列表' ;?></h1>
				<p class="wow bounceInRight pt-2 px-3"><?php echo grace_option('users_description') ? grace_option('users_description') : ''; ?></p>
			</div>
		</div>
	</div>
	<div class="container pt-4">
		<div class="category-list row ">
			<?php
				$number = intval(grace_option('users_number'));
				
				$admin = get_users(
		            array(
		                'blog_id'=> $GLOBALS['blog_id'],
		                'orderby' => 'post_count',
		                'order' => 'DESC',
		                'role' => 'administrator'
		            )
		        );//获取管理员
		        
				$editors = get_users(
		            array(
		                'blog_id'=> $GLOBALS['blog_id'],
		                'orderby' => 'post_count',
		                'order' => 'DESC',
		                'role' => 'editor'
		            )
		        );//获取编辑
		        $authors = get_users(
		            array(
		                'blog_id' => $GLOBALS['blog_id'],
		                'orderby' => 'post_count',
		                'order' => 'DESC',
		                'role' => 'author'
		            )
		        );//获取作者
		        $contributors = get_users(
		            array(
		                'blog_id' => $GLOBALS['blog_id'],
		                'orderby' => 'post_count',
		                'order' => 'DESC',
		                'role' => 'contributor'
		            )
		        );//获取投稿者
		        $users = array_merge($admin,$editors,$authors,$contributors);//使用array_merge()将两个或多个数组的单元合并起来
		        $query = array_slice($users, 0, $number);//array_slice() 函数在数组中根据条件取出一段值，并返回
				
				foreach ($query as $user) { ?>					
					<article class="col-md-3 users-item text-center">
						<div class="alpha-item">							
							<a href="<?php echo get_author_posts_url($user->ID); ?>">
							<div class="authorimg">
			        		<?php echo get_avatar( get_the_author_meta( 'user_email',$user->ID ) ,115 ); ?>
			        		</div></a>
							<div class="p-3">
								<h3 class="text-center"><a href="<?php echo get_author_posts_url($user->ID); ?>" class="text-dark"><?php echo strip_tags(get_the_author_meta('display_name',$user->ID)); ?></a></h3>
								<p class="text-secondary center-content"><?php if (get_the_author_meta('description',$user->ID)) { echo strip_tags(get_the_author_meta('description',$user->ID));} else {_e('The person is so lazy that he left nothing.','grace');} ?></p>
							</div>
						</div>
					</article>					
				<?php }?>
		</div>
	</div>
</main>
<?php get_footer(); ?>